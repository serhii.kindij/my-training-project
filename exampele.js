// One-page applications 
// Показати одну сторінку та приховати дві інші
function showPage(page) {
  // Сховати всі div:
  document.querySelectorAll('.page-app-div').forEach(div => {
      div.style.display = 'none';
  });
  // Показати div, переданий у аргументі функції
  document.querySelector(`#${page}`).style.display = 'block';
}

// Показати наданий розділ
function showSection(section) {              
  // Знайти текст розділу з сервера
  fetch(`./sections/${section}`)
    .then(response => response.text())
    .then(text => {
      // Записати текст та вивести на сторінці
      console.log(text);
      document.querySelector('#page-content').innerHTML = text;
    });
}

// Зачекати завантаження сторінки:
document.addEventListener('DOMContentLoaded', function() {
  // Обрати потрібні кнопки
  document.querySelectorAll('.page-app-btn').forEach(button => {
      // Коли кнопку натиснуто, перейти на сторінку
      button.onclick = function() {
          showPage(this.dataset.page);
          showSection(this.dataset.section);
      }
  })
});

// Зачекати завантаження сторінки
document.addEventListener('DOMContentLoaded', function() {

  let greetingsToUkraine = document.getElementById('greetUkraine');
    greetingsToUkraine.addEventListener('click', () => {
      alert('Привіт, Україно!!!');
    })

    let animationBtn = document.getElementById('animationBtn');
      // Призупинити анімацію за замовчуванням
      greetingsToUkraine.style.animationPlayState = 'paused';

    // Дочекатись, поки кнопка буде натиснута
    animationBtn.onclick = () => {
      // Якщо анімація призупинена, програти анімацію
      if (greetingsToUkraine.style.animationPlayState == 'paused') {
        greetingsToUkraine.style.animationPlayState = 'running';
      }
      // В іншому випадку, призупинити анімацію
      else {
        greetingsToUkraine.style.animationPlayState = 'paused';
      }
    }

// ========================================================
  let counterOnPage = document.getElementById('btnCounter');
  let clearCounter = document.getElementById('clearCounter');
  

  // Перевірити, чи вже є значення у локальному сховищі
  if (!localStorage.getItem('counter')) {
    // Якщо ні, встановити лічильник на нуль у локальному сховищі
    localStorage.setItem('counter', 0);
  }

    function Count() {
      // Отримати значення лічильника з локального сховища
      let pageCounter = localStorage.getItem('counter');
        // Оновити лічильник
        pageCounter++;
        document.querySelector('.counter').innerHTML = pageCounter;
         // Зберегти лічильник у локальному сховищі
         localStorage.setItem('counter', pageCounter);

      if (pageCounter % 7 === 0) {
        alert(`Зараз на лічильнику буде ${pageCounter} балів`)
      }
    }

  function ResetCounter() {
    pageCounter = 0;
    localStorage.setItem('counter', pageCounter);
    document.querySelector('.counter').innerHTML = 0;
  }

  // Встановити заголовок таким, що дорівнює поточному значенню у локальному сховищі
  document.querySelector('.counter').innerHTML = localStorage.getItem('counter');

    counterOnPage.addEventListener('click', Count);
    clearCounter.addEventListener('click', ResetCounter);

// ========================================================
  let magicTitle = document.getElementById('magicTitle');

    function EnchantTitle() {
      const header3 = document.querySelector('h3');
        if (header3.innerHTML === 'чарівний заголовок') {
            header3.innerHTML = 'дійсно, так і є';
        }
        else {
            header3.innerHTML = 'чарівний заголовок';
        }
    }
  
    magicTitle.addEventListener('click', EnchantTitle);

// ========================================================
  document.querySelector('#personGreet').onsubmit = function() {
    const name = document.querySelector('#name').value;
      alert(`Привіт, ${name}`);
      return false;
    };

// ========================================================
  document.querySelectorAll('.color-btn').forEach(function(button) {
    button.onclick = function() {
      document.querySelector("#colorH2").style.color = button.dataset.color;
    }
  });

  document.querySelector('select').onchange = function() {
    document.querySelector('#colorH3').style.color = this.value;
  } 
 
//create a to-do list
  // Отримати кнопку надсилання та введення для подальшого використання 
  const submit = document.querySelector('#submit');
  const newTask = document.querySelector('#task');

  // За замовчуванням вимкнути кнопку надсилання:
  submit.disabled = true;

  // Слухати подію вводу даних до поля введення:
  newTask.onkeyup = () => {
    if (newTask.value.length > 0) {
      submit.disabled = false;
    }
    else {
      submit.disabled = true;
    }
  }

  // Слухати подію надсилання форми
  document.querySelector('#taskForm').onsubmit = () => {
    // Знайти завдання, яке додав користувач
    const task = newTask.value;
    // Створити пункт списку для нового завдання та додати завдання до нього
    const li = document.createElement('li');
      li.innerHTML = task;
    // Додати новий елемент до невпорядкованого списку:
    document.querySelector('#tasks').append(li);
      // Очистити поле введення:
      newTask.value = '';
      // Знову вимкнути кнопку надсилання:
      submit.disabled = true;
        // Зупинити надсилання форми:
        return false;
    }
})

// Попрацюємо над програмою, за допомогою якої можна знайти курси обміну між двома валютами
// ми будемо використовувати API обмінного курсу Європейського центрального банку.
document.addEventListener('DOMContentLoaded', function() {
  document.querySelector('#CurrencyExchange').onsubmit = function() {
    // Надіслати запит GET до URL
    fetch('https://api.exchangeratesapi.io/latest?base=USD')
      // Перетворити відповідь у формат json
      .then(response => response.json())
      .then(data => {
        // Отримати валюту від користувача та перевести у верхній регістр 
        const currency = document.querySelector('#currency').value.toUpperCase();
          // Отримати курс з даних
          const rate = data.rates[currency];
            // Перевірити, чи валюта дійсна:
            if (rate !== undefined) {
              // Показати курс обміну на екрані
              document.querySelector('#CurrencyResult').innerHTML = `1 долар дорівнює ${rate.toFixed(3)} ${currency}.`;
            }
            else {
              // Показати помилку на екрані
              document.querySelector('#CurrencyResult').innerHTML = 'Недійсна валюта.';
            }
      })
    // Спіймати всі помилки та вивести їх до консолі
    .catch(error => {
        console.log('Помилка:', error);
    });
    // Запобігти надсиланню за замовчуванням
    return false;
}
});
// ===============================================================================

 // Слухач події прокручування
 window.onscroll = () => {

  // Перевірити, чи ми внизу сторінки
  if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {

      // Змінити колір фону на зелений
      document.querySelector('body').style.background = 'green';
  } else {

      // Змінити колір фону на білий
      document.querySelector('body').style.background = 'PaleGoldenrod';
  }

};